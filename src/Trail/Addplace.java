package Trail;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import org.testng.Assert;

import Body.playload;


public class Addplace {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RestAssured.baseURI="https://rahulshettyacademy.com";
		//add place//get place//update place
		///post the address
	String response=	given().log().all().queryParam("key", "qaclick123").header("Content-Type","application/json")
		.body(playload.AddPlace()).when().post("/maps/api/place/add/json").then().assertThat().statusCode(200)
		.body("scope", equalTo("APP")).header("Server", "Apache/2.4.41 (Ubuntu)")
		.extract().response().asString();
	System.out.println(response);
	JsonPath jspath = new JsonPath(response);
	String placeid=jspath.getString("place_id");
	System.out.println(placeid);
	
	//updateplace
	String newaddress="70 Summer walk,four street USA"; 
	given().log().all().queryParam("key", "qaclick123").header("Content-Type","application/json")
	.body("{\r\n"
			+ "\"place_id\":\""+placeid+"\",\r\n"
			+ "\"address\":\""+newaddress+"\",\r\n"
			+ "\"key\":\"qaclick123\"\r\n"
			+ "}").when().put("/maps/api/place/update/json").then().log().all().assertThat().statusCode(200)
	.body("msg", equalTo("Address successfully updated"));
	
	//getplace
	String getplace=given().log().all().queryParam("key", "qaclick123").queryParam("place_id", placeid)
	.when().get("/maps/api/place/get/json").then().log().all().assertThat().statusCode(200)
	.extract().response().asString();
	
	
	JsonPath jspath1= new JsonPath(getplace);
	String actualaddress=jspath1.getString("address");
	System.out.println(actualaddress);
		
	Assert.assertEquals(actualaddress, newaddress);

	}

}
