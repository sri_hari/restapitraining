package Trail;

import static io.restassured.RestAssured.given;

import org.testng.annotations.Test;

import Body.Reusable;
import Body.playload;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class BooksAdd {
	@Test
	public void PostBook() {
		RestAssured.baseURI="https://216.10.245.166";
		String response= given().log().all().header("Content-Type","application/json").body(playload.Addbook())
		 .when().post("/Library/Addbook.php").then().log().all().assertThat().statusCode(200)
		 .extract().response().asString();
		JsonPath js=Reusable.rawTojson(response);
		String id=js.get("ID");
		System.out.println(id);
	}

}
