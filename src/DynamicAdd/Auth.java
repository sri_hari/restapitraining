package DynamicAdd;
import static io.restassured.RestAssured.*;
import static  org.hamcrest.Matchers.*;
import org.testng.annotations.Test;

public class Auth {
	
	@Test(priority = 1)
	public void BasicAuth() {
		given().auth().basic("postman", "password")
		.when().get("https://postman-echo.com/basic-auth")
		.then().statusCode(200)
		.body("authenticated", equalTo(true))
		.log().all();
		
	}
	@Test(priority = 2)
	public void DigestAuth() {
		given().auth().digest("postman", "password")
		.when().get("https://postman-echo.com/basic-auth")
		.then().statusCode(200)
		.body("authenticated", equalTo(true))
		.log().all();
		
	}
	@Test(priority = 3)
	public void PreemptiveAuth() {
		given().auth().preemptive().basic("postman", "password")
		.when().get("https://postman-echo.com/basic-auth")
		.then().statusCode(200)
		.body("authenticated", equalTo(true))
		.log().all();
		
	}



}
