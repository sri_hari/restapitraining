package DynamicAdd;
import static io.restassured.RestAssured.*;
import static  org.hamcrest.Matchers.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.testng.annotations.Test;

public class Trail1 {
	@Test(priority = 1)

	public void GetEmployee() {
		when().get("http://localhost:3000/EmployeeDetails")
		.then().log().all().statusCode(200);	
		
	}
//	/Using HasMap Concepts
	//@Test(dependsOnMethods = "GetEmployee")
	public void EmployeeAdd() {
		HashMap hash = new HashMap();
		hash.put("name","karthi");
		hash.put("age","53");
		hash.put("Gender", "M");
		String skills[]= {"python","api"};
		hash.put("Techskills", skills);
		given().contentType("application/json")
		.body(hash).when().post("http://localhost:3000/EmployeeDetails")
		.then()
		.statusCode(201)
		.body("name",equalTo("karthi"))
		.body("age",equalTo("53"))
		.body("Techskills[0]",equalTo("python"))
		
		.header("Content-Type", "application/json; charset=utf-8")
		.log().all();
	}
	//@Test(priority = 3)
	public void DeleteEmployee() {
		when().delete("http://localhost:3000/EmployeeDetails/2")
		.then().log().status();
	}
	
	///Using JsonObject 
//	@Test
	public void EmployeeaddJson() {
		JSONObject hash = new JSONObject();
		hash.put("name","hari");
		hash.put("age","23");
		hash.put("Gender", "M");
		String skills[]= {"javascript","api"};
		hash.put("Techskills", skills);
		given().contentType("application/json")
		.body(hash.toString()).when().post("http://localhost:3000/EmployeeDetails")
		.then()
		.statusCode(201)
		.body("name",equalTo("hari"))
		.body("age",equalTo("23"))
		.body("Techskills[0]",equalTo("javascript"))
		
		.header("Content-Type", "application/json; charset=utf-8")
		.log().all();
	}
	//Using Pojoclass
	//@Test
	public void EmployeeaddPojo() {
		PojoClass pojo = new PojoClass();
		pojo.setName("edwin");
		pojo.setAge("40");
		pojo.setGender("M");
		String skil[]= {"java","c#"};
		pojo.setTechskills(skil);
		given().contentType("application/json")
		.body(pojo).when().post("http://localhost:3000/EmployeeDetails")
		.then()
		.statusCode(201)
		.body("name",equalTo("edwin"))
		.body("age",equalTo("40"))
		.body("Techskills[0]",equalTo("java"))
		
		.header("Content-Type", "application/json; charset=utf-8")
		.log().all();
	}
	//Using External Json file
	@Test
	public void EmployeeaddFromExternalFile() throws FileNotFoundException {
		File fe=new File(".\\body.json");
		FileReader fr = new FileReader(fe);
		JSONTokener jt=new JSONTokener(fr);
		JSONObject jb=new JSONObject(jt);
	
		given().contentType("application/json")
		.body(jb.toString()).when().post("http://localhost:3000/EmployeeDetails")
		.then()
		.statusCode(201)
		.body("name",equalTo("nithis"))
		.body("age",equalTo("25"))
		.body("Techskills[0]",equalTo("java"))
		
		.header("Content-Type", "application/json; charset=utf-8")
		.log().all();
	}
}
