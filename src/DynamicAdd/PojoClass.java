package DynamicAdd;

public class PojoClass {
	String name;
	String age;
	String Gender;
	String Techskills[];
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getGender() {
		return Gender;
	}
	public void setGender(String gender) {
		Gender = gender;
	}
	public String[] getTechskills() {
		return Techskills;
	}
	public void setTechskills(String[] techskills) {
		Techskills = techskills;
	}
	
}
